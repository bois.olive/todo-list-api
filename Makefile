.PHONY: default run build tests tests-e2e generate_swagger

# loading env if file exists
ifeq ($(shell test -s .env && echo -n yes),yes)
	include .env
	export $(shell sed 's/=.*//' .env)
endif

STAGING_HOST="todo-list-api.endpoints.staging.cloud.goog"

PRODUCTION_HOST="todo-list-api.endpoints.prod.cloud.goog"


default: build

run: tests tests-e2e
	go run main.go

build: tests tests-e2e
	@echo "building Get things done app..."
	go build -o GetThingsDone
	@echo "built executable 'GetThingsDone'"


tests :
	go test $(shell go list ./... | grep -v e2e) -count=1 -p 1

tests-e2e :
	go test -v -count=1 ./internal/e2e -count=1

generate_swagger:
	rm -f ./deployments/swagger/*
	@swag init -o ./deployments/swagger/ -g main.go
	cp ./deployments/swagger/swagger.json ./deployments/swagger/production.swagger.json
	sed "s/$(PRODUCTION_HOST)/$(STAGING_HOST)/" ./deployments/swagger/production.swagger.json > ./deployments/swagger/staging.swagger.json
	rm -f ./deployments/swagger/docs.go ./deployments/swagger/swagger.json ./deployments/swagger/swagger.yaml


