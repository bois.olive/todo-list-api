package bootstrap

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/bois.olive/todo-list-api/internal/domain/todo/api"
	"gitlab.com/bois.olive/todo-list-api/internal/domain/todo/port"
	"gitlab.com/bois.olive/todo-list-api/internal/infrastructure"
	"gitlab.com/bois.olive/todo-list-api/internal/ui"
	"os"
)

func InitApp() *gin.Engine {
	router, _ := launchApp("/tmp/Todos.db", false)
	return router
}

func InitAppForIntegrationTest() (*gin.Engine, port.ITodosRepository) {
	return launchApp("/tmp/TodosTest.db", true)
}

func launchApp(url string, drop bool) (*gin.Engine, port.ITodosRepository) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})

	repository, err := infrastructure.NewTodosRepository(url, drop)
	if err != nil {
		log.Fatal().Err(err).Msg("Error during get Things done initialization")
	}
	todosAPI := api.NewApi(repository)
	router := ui.NewRouter(todosAPI)

	return router, repository
}
