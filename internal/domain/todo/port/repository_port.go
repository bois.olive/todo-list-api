package port

import "gitlab.com/bois.olive/todo-list-api/internal/domain/todo/model"

const (
	NoRowDeleted = "no row deleted"
)

type ITodosRepository interface {
	ReadTodo(int) (model.Todo, error)
	ReadTodoList() ([]model.ISummaryResponse, error)
	Create(todo model.Todo) (int, error)
	UpdateTodo(todo model.Todo) error
	DeleteTodo(id int) error
	EmptyDatabaseForTests()
}
