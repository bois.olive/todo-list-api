package model

import (
	"time"
)

type Todo struct {
	ID           int
	CreationDate time.Time
	Description  string
	DueDate      time.Time
	Title        string
}

func (t Todo) MapToTodoResponse() ReadTodoResponse {
	if t.ID == 0 {
		return ReadTodoResponse{}
	}
	return ReadTodoResponse{
		ID:           t.ID,
		Title:        t.Title,
		Description:  t.Description,
		CreationDate: t.CreationDate,
		DueDate:      t.DueDate,
	}
}
