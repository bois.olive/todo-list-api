# todo-list-api



## Getting started


### run
```shell script
make run
```

### automated tests
```shell script
make tests
```
# Usage :

endpoints URL is

http://localhost:8080/todos

### Create
```shell script
curl -i -X POST \
  http://localhost:8080/todos \
  -H 'Content-Type: application/json' \
  -d '{
   "title":"plant a tree",
   "description" : "because green is pleasing",
   "dueDate": 1557847007
}'
```
### Update
```shell script
curl -i -X PUT \
  http://localhost:8080/todos/1 \
  -H 'Content-Type: application/json' \
  -d '{
   "title":"cut a tree",
   "description" : "to burn it un my fireplace",
   "dueDate": 155784755
}'
```
### Delete
```shell script
curl -i -X DELETE http://localhost:8080/todos/5
```
### Read todo
```shell script
curl -i -X GET http://localhost:8080/todos/1
```
### list
```shell script
curl -X GET http://localhost:8080/todos
```
